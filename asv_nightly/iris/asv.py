"""
Script to interpret a list (asv_repos.yml) of repositories, branches and
benchmarks to run in the branches.

First acquires the correct suite of iris test data for use in the benchmarks.
"""


import git
import json
import os
from pathlib import Path
import re
from shutil import rmtree
from subprocess import run
from urllib.request import urlretrieve
from warnings import warn
import yaml


def manage_repo(name, source_url, target_dir):
    """Return a specified repo at a specified path, cloning first if necessary."""
    subdir = target_dir / name
    repo = None
    try:
        repo = git.Repo(subdir)
    except (git.InvalidGitRepositoryError, git.NoSuchPathError) as err:
        print(f"Could not find git repository: {subdir}, cloning afresh")
        if subdir.exists():
            rmtree(subdir)
        subdir.mkdir(parents=True)
        try:
            repo = git.Repo.clone_from(source_url, subdir)
        except git.exc.GitCommandError:
            warn(f"Error cloning {source_url}.")

    if repo:
        # Avoid detached head state.
        repo.git.checkout(".")

    return repo, subdir


def repoint_repo(asv_conf_path, repo_path):
    """Modify the 'repo' item in a specified copy of asv.conf.json to be the
    specified path."""
    # Read in the asv configuration JSON.
    with open(asv_conf_path, 'r') as asv_conf:
        asv_conf_lines = asv_conf.readlines()

    # Standardise the JSON format.
    json_string = ""
    for line in asv_conf_lines:
        if not line.strip().startswith("//"):
            json_string = "".join([json_string, line])
    pattern = re.compile(",\n *}")
    json_string = pattern.sub("}", json_string)

    # Edit the asv configuration file to point at the specified repo.
    json_content = json.loads(json_string)
    json_content["repo"] = repo_path
    with open(asv_conf_path, "w") as asv_conf:
        json.dump(json_content, asv_conf)


asv_dir = Path(os.environ["ASV_DIR"])
results_dir = Path(os.environ["RESULTS_DIR"])

# Download the latest travis config file from SciTools Iris repository.
travis_url = \
    "https://raw.githubusercontent.com/SciTools/iris/master/.travis.yml"
travis_dest = asv_dir / "iris_travis.yml"
if travis_dest.exists():
    travis_dest.unlink()
urlretrieve(travis_url, travis_dest)

# Find the commit hash that travis uses for the Iris Test Data.
travis_str_search = "IRIS_TEST_DATA_REF="
with open(travis_dest, "r") as travis_file:
    yaml_content = travis_file.read()
    ix_start = yaml_content.index(travis_str_search) + len(travis_str_search)
    ix_end = yaml_content.index(";", ix_start)
    iris_test_data_hash = yaml_content[ix_start:ix_end]
iris_test_data_hash = iris_test_data_hash.strip('\"')

# Fetch Iris Test Data, check out the specified commit hash.
test_data_repo, test_data_dir = \
    manage_repo("iris-test-data",
                "https://github.com/SciTools/iris-test-data.git",
                asv_dir)
test_data_repo.git.checkout("master")
test_data_repo.git.pull("origin")
test_data_repo.git.checkout(iris_test_data_hash)
os.environ["OVERRIDE_TEST_DATA_REPOSITORY"] = str(test_data_dir / "test_data")

# Read the repo list from the configuration file.
own_dirpath = Path(__file__).parent
yaml_path = own_dirpath / "asv_repos.yml"
with open(yaml_path, 'r') as stream:
    repo_list = yaml.load(stream, Loader=yaml.BaseLoader)
repos_dir = asv_dir / "repos"

for repo_dict in repo_list:
    # Iterate through the listed repos.
    repo_name = repo_dict["name"]
    repo, repo_subdir = manage_repo(repo_name, repo_dict["metrics git"], repos_dir)
    if repo is None:
        warn(f"Upstream error identifying repo for: {repo_name} - "
             f"skipping.")
        continue

    benchmark_subdir = repo_subdir / "asv_projects" / "iris"
    os.chdir(benchmark_subdir)

    branch_list = repo_dict["metrics branches"]
    for branch_dict in branch_list:
        # Checkout each listed branch in the current repo.
        branch_name = branch_dict["name"]
        try:
            if branch_name in repo.branches:
                repo.git.checkout(branch_name)
                repo.git.pull("origin")
            else:
                repo.git.fetch()
                repo.git.checkout(branch_name)
        except git.exc.GitCommandError:
            warn(f"Error checking out and pulling branch: '{branch_name}' in"
                 f" repo: {repo_name} - skipping.")
            continue

        # Create sub-directory for publishing the results from this branch.
        results_subdir = results_dir / repo_name / branch_name
        if not results_subdir.exists():
            results_subdir.mkdir(parents=True)

        # Adjust ASV's config to benchmark against the desired repository.
        asv_conf_path = benchmark_subdir / "asv.conf.json"
        repoint_repo(asv_conf_path, repo_dict["iris git"])

        # Perform "asv run" with each of the argument strings listed.
        run_list = branch_dict["asv run arguments"]
        for run_args in run_list:
            print(f"RUNNING repo: {repo_name}, branch: {branch_name}, "
                  f"asv run args: {run_args}")
            run(f"asv run {run_args}", shell=True)

        # Publish all results for this branch.
        run(["asv", "publish", "--html-dir", results_subdir])

        # Undo any changes made to the repo (e.g. asv.conf.json).
        repo.git.checkout(".")

print("BENCHMARKING COMPLETE")
