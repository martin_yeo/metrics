"""
Take the compressed standard FF data file, and save it as a compressed PP file.

Note: use mule, because Iris save of compressed PP fields seems to be broken
(at Iris 2.4).

"""
import mule
from mule.pp import fields_to_pp_file

import os
from pathlib import Path

_DATADIR_VARNAME = 'BENCHMARK_DATA'  # For local runs

benchmark_data_dir = os.environ.get(_DATADIR_VARNAME, None)
if benchmark_data_dir is None:
    msg = ('Benchmark data dir is not defined : "${}" must be set.')
    raise(ValueError(msg.format(_DATADIR_VARNAME)))

benchmark_data_dir = Path(benchmark_data_dir)

in_path = str(benchmark_data_dir / 'umglaa_pb000-theta')
out_path = str(benchmark_data_dir / 'umglaa_pb000-theta.compressed.pp')

ff = mule.FieldsFile.from_file(in_path)
fields_to_pp_file(out_path, ff.fields)
