from tehuti import TimeMetric
import biggus
import netCDF4


# Helper function
def mean(context, axis):
    mean = biggus.mean(context.array, axis=axis)
    biggus.engine.ndarrays(mean)


# 5.5GB classic netCDF
def tas_setup(context):
    path = '/data/local/dataZoo/NetCDF/largeFile/' \
           'tas_day_HadGEM2-ES_historical_r1i1p1_18591201-20051130.nc'
    nc_var = netCDF4.Dataset(path).variables['tas']
    context.array = biggus.OrthoArrayAdapter(nc_var)
    #context.array = context.array[:400]


def tas_mean_axis_0(context):
    mean(context, 0)


def tas_mean_axis_1(context):
    mean(context, 1)


def tas_mean_axis_2(context):
    mean(context, 2)


def tas_std_axis_0(context):
    std = biggus.std(context.array, axis=0)
    biggus.engine.ndarrays(std)


def tas_mean_std_axis_0(context):
    mean = biggus.mean(context.array, axis=0)
    std = biggus.std(context.array, axis=0)
    biggus.engine.ndarrays(mean, std)


# 34GB classic netCDF
def ta_setup(context):
    path = '/data/local/dataZoo/NetCDF/largeFile/' \
           'ta_6hrPlev_HadGEM2-ES_historical_r1i1p1_1950010106-2006010100.nc'
    nc_var = netCDF4.Dataset(path).variables['ta']
    context.array = biggus.OrthoArrayAdapter(nc_var)
    #context.array = context.array[:400]


def ta_mean_axis_0(context):
    mean(context, 0)


def ta_mean_axis_2(context):
    mean(context, 2)


def ta_mean_axis_3(context):
    mean(context, 3)


metrics = [
    TimeMetric(tas_mean_axis_0, tas_setup, repeat=1),
    TimeMetric(tas_mean_axis_1, tas_setup, repeat=1),
    TimeMetric(tas_mean_axis_2, tas_setup, repeat=1),

    TimeMetric(tas_std_axis_0, tas_setup, repeat=1),

    TimeMetric(tas_mean_std_axis_0, tas_setup, repeat=1),

    TimeMetric(ta_mean_axis_0, ta_setup, repeat=1),
    TimeMetric(ta_mean_axis_2, ta_setup, repeat=1),
    TimeMetric(ta_mean_axis_3, ta_setup, repeat=1),
    ]
